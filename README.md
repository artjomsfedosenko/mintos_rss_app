In order to setup the app:
1) Clone the project from Git repository by executing: git clone url_to_repository
2) Navigate to project folder and install dependencies using composer by executing following command: composer install
If you don`t have composer install it globally or get the local version.
3) Sequentially run scripts from db_scripts.sql file on your MySQL database.
4) In app/parameters.yml change DB related parameters to point to your database.
5) In order to check if app is working use Symfony`s built in web server by running: symfony server:start
Application should be available on localhost:8000.