<?php


namespace Tests\AppBundle\Service;

use App\Util\Calculator;
use AppBundle\Service\HttpService;
use PHPUnit\Framework\TestCase;
use RuntimeException;

class HttpServiceTest extends TestCase
{
    public function testMakeRequest()
    {
        $httpService = new HttpService();
        $result = $httpService->makeRequest("http://google.lv");

        $this->assertStringStartsWith("<!doctype html>", $result);
    }

    public function testMakeRequestThrowsException()
    {
        $this->expectException(RuntimeException::class);

        $httpService = new HttpService();
        $httpService->makeRequest("http://google.lvs");
    }
}