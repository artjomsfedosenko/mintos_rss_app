<?php


namespace Tests\AppBundle\Service;


use AppBundle\Service\MostCommonWordsCounterService;
use PHPUnit\Framework\TestCase;
use SimpleXMLElement;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;

class MostCommonWordsCounterServiceTest extends KernelTestCase
{
    /** @var Container */
    private $container;

    public function setUp()
    {
        self::bootKernel();
        $this->container = self::$kernel->getContainer();
    }

    public function testCountWordsReturnsExpectedResult()
    {
        $mostCommonWords = $this->container->getParameter('most_common_words');
        $wordCount = $this->container->getParameter('selected_word_count');
        $xml = new SimpleXMLElement($this->feedContent);

        $service = new MostCommonWordsCounterService($mostCommonWords, $wordCount);
        $result = $service->getWords($xml);

        $expectedResult = [
            "register" => 2,
            "windows" => 2,
            "has" => 2,
            "ceo" => 2,
            "independent" => 2,
            "microsoft" => 2,
            "tuned" => 1,
            "setups" => 1,
            "finely" => 1,
            "anderson" => 1,
        ];
        $this->assertEquals($expectedResult, $result);
    }

    private $feedContent = '<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="en">
  <id>tag:theregister.co.uk,2005:feed/theregister.co.uk/software/</id>
  <title>The Register - Software</title>
  <link rel="self" type="application/atom+xml" href="https://www.theregister.co.uk/software/headlines.atom"/>
  <link rel="alternate" type="text/html" href="https://www.theregister.co.uk/software/"/>
  <rights>Copyright © 2019, Situation Publishing</rights>
  <author>
    <name>Team Register</name>
    <email>webmaster@theregister.co.uk</email>
    <uri>https://www.theregister.co.uk/odds/about/contact/</uri>
  </author>
  <icon>https://www.theregister.co.uk/Design/graphics/icons/favicon.png</icon>
  <subtitle>Biting the hand that feeds IT — sci/tech news and views for the world</subtitle>
  <logo>https://www.theregister.co.uk/Design/graphics/Reg_default/The_Register_r.png</logo>
  <updated>2019-05-24T12:11:06Z</updated>
  <entry>
    <id>tag:theregister.co.uk,2005:story203104</id>
    <updated>2019-05-24T12:11:06Z</updated>
    <author>
      <name>Richard Speed</name>
      <uri>https://search.theregister.co.uk/?author=Richard%20Speed</uri>
    </author>
    <link rel="alternate" type="text/html" href="http://go.theregister.com/feed/www.theregister.co.uk/2019/05/24/windows_10_system_configuration/"/>
    <title type="html">Headsup for those managing Windows 10 boxen: Microsoft has tweaked patching rules</title>
    <summary type="html" xml:base="http://www.theregister.co.uk/">&lt;h4&gt;One category to rule them all? Er, maybe not...&lt;/h4&gt; &lt;p&gt;Administrators dealing with the rollout of Microsoft\'s latest and greatest Windows 10 were warned last night that some tinkering of their finely tuned setups would be required.…&lt;/p&gt;</summary>
  </entry>
  <entry>
    <id>tag:theregister.co.uk,2005:story203088</id>
    <updated>2019-05-24T08:10:13Z</updated>
    <author>
      <name>Tim Anderson</name>
      <uri>https://search.theregister.co.uk/?author=Tim%20Anderson</uri>
    </author>
    <link rel="alternate" type="text/html" href="http://go.theregister.com/feed/www.theregister.co.uk/2019/05/24/github_is_independent_despite_microsoft_acquisition_insists_ceo/"/>
    <title type="html">Microsoft? Oh it\'s just another partnership, insists GitHub CEO</title>
    <summary type="html" xml:base="http://www.theregister.co.uk/">&lt;h4&gt;We\'re a strong, independent company&lt;/h4&gt; &lt;p&gt;"GitHub has to be both independent and neutral," CEO Nat Friedman said at the company\'s Satellite event in Berlin – despite its acquisition by Microsoft in October 2018.…&lt;/p&gt;</summary>
  </entry>
  </feed>';
}