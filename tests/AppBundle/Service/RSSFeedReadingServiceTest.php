<?php


namespace Tests\AppBundle\Service;


use AppBundle\Service\HttpService;
use AppBundle\Service\RSSFeedReadingService;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\DependencyInjection\Container;

class RSSFeedReadingServiceTest extends KernelTestCase
{
    /** @var Container */
    private $container;

    public function setUp()
    {
        self::bootKernel();
        $this->container = self::$kernel->getContainer();
    }

    public function testRSSFeedReaderReturnsValidXMLRepresentation()
    {
        $logger = $this->container->get('logger');
        $service = new RSSFeedReadingService(new HttpService(), $logger);
        /** @var \SimpleXMLElement $document */
        $result = $service->getFeed("http://theregister.co.uk/software/headlines.atom");

        $title = $result->children()[1];
        $this->assertEquals("The Register - Software", $title);
    }

    public function testRSSFeedReaderReturnsEmptyDocumentOnWhenEncountersError()
    {
        $logger = $this->container->get('logger');
        $service = new RSSFeedReadingService(new HttpService(), $logger);
        /** @var \SimpleXMLElement $document */
        $result = $service->getFeed("tst");

        $this->assertNull($result);
    }
}