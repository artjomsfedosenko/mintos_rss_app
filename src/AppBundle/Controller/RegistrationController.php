<?php


namespace AppBundle\Controller;


use AppBundle\Form\UserType;
use AppBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use AppBundle\Entity\User;


class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="register")
     */
    public function registerAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);
            $this->getDoctrine()
                ->getRepository(User::class)
                ->saveUser($user);

            return $this->redirectToRoute('login');
        }

        return $this->render(
            'registration/register.html.twig',
            ['form' => $form->createView()]
        );
    }

    /**
     * @Route("/checkEmailRegistered", name="checkEmailRegistered")
     */
    public function checkEmailRegisteredAction(Request $request)
    {
        $email = $request->request->get("email");
        if(is_null($email) || !$this->isEmail($email)){
            return new JsonResponse(['isInvalid' => true]);
        }

        $user = $this->getDoctrine()
            ->getRepository(User::class)
            ->loadUserByUsername($email);

        if($user){
            return new JsonResponse(['isRegistered' => true]);
        }

        return new JsonResponse(['isRegistered' => false]);
    }

    private function isEmail(string $email) : bool
    {
        return boolval(filter_var($email, FILTER_VALIDATE_EMAIL));
    }

}