<?php


namespace AppBundle\Controller;


use AppBundle\Entity\FeedItem;
use AppBundle\Service\MostCommonWordsCounterService;
use AppBundle\Service\RSSFeedReadingService;
use SimpleXMLElement;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class RSSFeedController extends Controller
{
    private $rssReader;

    private $mostCommonWordsCounter;

    public function __construct(RSSFeedReadingService $rssReader, MostCommonWordsCounterService $mostCommonWordsCounter)
    {
        $this->rssReader = $rssReader;
        $this->mostCommonWordsCounter = $mostCommonWordsCounter;
    }


    /**
     * @Route("/", name="home")
     */
    public function displayRSSFeedAction(Request $request)
    {
        $url = "http://theregister.co.uk/software/headlines.atom";
        $rawFeed = $this->rssReader->getFeed($url);

        if(is_null($rawFeed)){
            return $this->render('feed/feed.html.twig', [
                'mostCommonWords' => [],
                'feed' => [],
            ]);
        }

        $rawFeed->registerXPathNamespace("xs", "http://www.w3.org/2005/Atom");
        $mostCommonWords = $this->mostCommonWordsCounter->getWords($rawFeed);
        $feed = $this->prepareFeedItems($rawFeed);

        return $this->render('feed/feed.html.twig', [
            'mostCommonWords' => $mostCommonWords,
            'feed' => $feed,
        ]);
    }

    private function prepareFeedItems(SimpleXMLElement $rawFeed) : array
    {
        $result = [];
        foreach ($rawFeed->xpath("//xs:entry") as $entry){
            $feedItem = new FeedItem(
                $entry->author->name,
                $entry->title,
                strip_tags($entry->summary),
                $entry->link['href']
            );

            $result[] = $feedItem;
        }

        return $result;
    }
}