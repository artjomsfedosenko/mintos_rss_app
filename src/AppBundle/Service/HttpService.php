<?php


namespace AppBundle\Service;

/**
 * Service intent is to make simple HTTP GET request to url and
 * return result as a string. Request is made using php-curl.
 */
use RuntimeException;

class HttpService
{
    public function makeRequest(string $url) : string
    {
        $curlSession = curl_init();
        $this->setCurlOptions($curlSession);
        curl_setopt($curlSession,CURLOPT_URL,$url);
        $result=curl_exec($curlSession);

        $errors = curl_error($curlSession);
        if($errors !== ""){
            throw new RuntimeException($errors);
        }
        curl_close($curlSession);

        return $result;
    }

    private function setCurlOptions($curlSession)
    {
        curl_setopt($curlSession,CURLOPT_RETURNTRANSFER,true);
        curl_setopt($curlSession,CURLOPT_HEADER, false);
        curl_setopt($curlSession,CURLOPT_FOLLOWLOCATION, true);
    }
}