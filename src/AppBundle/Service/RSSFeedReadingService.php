<?php


namespace AppBundle\Service;


use Psr\Log\LoggerInterface;
use SimpleXMLElement;
use Throwable;

/**
 * Service used to read RSS Feed from given url
 */
class RSSFeedReadingService
{
    private $httpService;

    private $logger;

    public function __construct(HttpService $httpService, LoggerInterface $logger)
    {
        $this->httpService = $httpService;
        $this->logger = $logger;
    }


    public function getFeed(string $url) : ?SimpleXMLElement
    {
        $xmlDoc = "";
        try{
            $xmlDoc = $this->httpService->makeRequest($url);
            return new SimpleXMLElement($xmlDoc);
        }catch(Throwable $exception){
            $this->logger->error("Could not retrieve data from :" . $url);
        }

        return null;
    }
}