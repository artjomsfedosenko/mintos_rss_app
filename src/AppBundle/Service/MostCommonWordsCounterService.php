<?php


namespace AppBundle\Service;


use SimpleXMLElement;
use Symfony\Component\DependencyInjection\Container;

/**
 * Service intent is to count all the word in provided XML structure and return
 * most common words with number of their occurrences in XML. Number of words returned
 * is specified by selected_word_count config parameter
 */
class MostCommonWordsCounterService
{
    private $resultingWords;

    private $mostCommonWords;

    private $wordCount;

    public function __construct(array $mostCommonWords, int $wordCount)
    {
        $this->mostCommonWords = $mostCommonWords;
        $this->wordCount = $wordCount;
    }

    public function getWords(SimpleXMLElement $xml) : array
    {
        //Validate input
        $this->resultingWords = [];
        if(is_null($xml)){
            return $this->resultingWords;
        }
        $this->processNodes($xml);

        return $this->getMostCommon();
    }

    private function processNodes(SimpleXMLElement $root) : void
    {
        /** @var SimpleXMLElement $node */
        foreach($root as $node){
            if($node->count() > 0){
                $this->processNodes($node);
            }else{
                $this->processSingleNode($node);
            }
        }
    }

    private function processSingleNode(SimpleXMLElement $node) : void
    {
        $decodedText = html_entity_decode($node[0]);
        $taglessText = strip_tags($decodedText);
        $separateWords = explode(" ", $taglessText);

        $this->countWords($separateWords);
    }

    private function countWords(array $separateWords) : void
    {
        /** @var string $word */
        foreach($separateWords as $word){
            $adjustedWord = strtolower($word);
            if($this->isWord($adjustedWord) && !$this->isWordCommon($adjustedWord)){
                $this->addToResult($adjustedWord);
            }
        }
    }

    private function isWord(string $word) : bool
    {
        return ctype_alnum($word) && !is_numeric($word);
    }

    private function isWordCommon(string $word) : bool
    {
        return in_array($word, $this->mostCommonWords);
    }

    private function addToResult(string $word) : void
    {
        if(array_key_exists($word, $this->resultingWords)){
            $this->resultingWords[$word]++;
        }else{
            $this->resultingWords[$word] = 1;
        }
    }

    private function getMostCommon() : array
    {
        arsort($this->resultingWords);

        return array_slice($this->resultingWords, 0, $this->wordCount);
    }
}