<?php


namespace AppBundle\Entity;


class FeedItem
{
    private $author;

    private $title;

    private $summary;

    private $link;

    /**
     * FeedItem constructor.
     * @param $author
     * @param $title
     * @param $summary
     * @param $link
     */
    public function __construct(string $author, string $title, string $summary, string $link)
    {
        $this->author = $author;
        $this->title = $title;
        $this->summary = $summary;
        $this->link = $link;
    }


    public function getAuthor() : string
    {
        return $this->author;
    }

    public function getTitle() : string
    {
        return $this->title;
    }

    public function getSummary() : string
    {
        return $this->summary;
    }

    public function getLink() : string
    {
        return $this->link;
    }
}