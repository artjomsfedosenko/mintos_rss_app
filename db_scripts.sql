CREATE SCHEMA IF NOT EXISTS rss_app;

CREATE USER application_user@localhost IDENTIFIED BY 'qwerty';
GRANT ALL PRIVILEGES ON rss_app.* TO application_user@localhost;

CREATE TABLE app_users(
   `id` INT NOT NULL AUTO_INCREMENT,
   `email` VARCHAR(64) NOT NULL,
   `password` VARCHAR(64) NOT NULL,
   PRIMARY KEY (`id`),
   CONSTRAINT `username_contr` UNIQUE (`email`)
);