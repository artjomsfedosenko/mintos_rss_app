$(document).on('input', '#user_email', function (event){
    var email = $(this).val();
    $.post("checkEmailRegistered", { email: email}, function (data) {
        var responseContainer = $('#emailMessage');
        responseContainer.text('');
        responseContainer.removeClass('message-error');

        if(data['isInvalid']){
            responseContainer.text('Email format invalid');
            responseContainer.addClass('message-error');
        }else if(data['isRegistered'] == true){
            responseContainer.text('Email already registered');
            responseContainer.addClass('message-error');
        }
    } );
});